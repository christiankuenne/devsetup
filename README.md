# My dev setup

Scripts to quickly setup an initial development or production environment. Works on Debian and Ubuntu.

## Getting started

Before running the scripts, update your configuration [here](./config.conf).

## Development

Run `./dev.sh`:

* Installs Node.js
* Installs .NET Core SDK
* Installs Docker and Docker Compose
* Configures git
* Configures npm

## Production

Run `./prod.sh`:

* Installs Docker and Docker Compose
