#!/bin/bash

# Check if sudo
if [ "$SUDO_USER" == '' ]
then
    echo 'Please run as super user.'
    exit 1
fi

# Source config
. config.conf

# Get Distro (Debian/Ubuntu), Release (buster/bionic) and Release Number (10/18.04)
DISTRO=$(echo "$(lsb_release -is)" | awk '{print tolower($0)}')
RELEASE=$(echo "$(lsb_release -cs)" | awk '{print tolower($0)}')
RELEASE_NUMBER=$(lsb_release -rs)

# Install utilities
apt install -y make apt-transport-https ca-certificates curl software-properties-common gnupg gnupg-agent

# Add Node.js repo
sudo -u $SUDO_USER curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
echo "deb https://deb.nodesource.com/node_$NODE_VERSION $RELEASE main" > /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/node_$NODE_VERSION $RELEASE main" >> /etc/apt/sources.list.d/nodesource.list

# Add Microsoft repo
if [[ $DISTRO == 'debian' ]]; then
    # Debian
    wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
    mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
    wget -q https://packages.microsoft.com/config/debian/$RELEASE_NUMBER/prod.list
    mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
    chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
    chown root:root /etc/apt/sources.list.d/microsoft-prod.list
else
    # Ubuntu
    wget -q https://packages.microsoft.com/config/ubuntu/$RELEASE_NUMBER/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    dpkg -i packages-microsoft-prod.deb
fi

# Add Docker repo
sudo -u $SUDO_USER curl -fsSL http://download.docker.com/linux/$DISTRO/gpg | apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/$DISTRO $RELEASE stable" > /etc/apt/sources.list.d/docker.list

# Update and install
apt update
apt install -y nodejs dotnet-sdk-3.1 docker-ce docker-ce-cli containerd.io

# Create docker group and add user
groupadd docker
usermod -aG docker $SUDO_USER

# Install Docker Compose
if [ ! -f /usr/local/bin/docker-compose ]
then
    curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
fi

# Set git and npm variables
sudo -u $SUDO_USER git config --global --replace-all user.name "$GIT_USER_NAME"
sudo -u $SUDO_USER git config --global --replace-all user.email "$GIT_USER_EMAIL"
sudo -u $SUDO_USER npm set init.author.name "$NPM_INIT_AUTHOR_NAME"
sudo -u $SUDO_USER npm set init.author.email "$NPM_INIT_AUTHOR_EMAIL"
