#!/bin/bash

# Check if sudo
if [ "$SUDO_USER" == '' ]
then
    echo 'Please run as super user.'
    exit 1
fi

# Remove Docker
apt remove -y docker docker-engine docker.io docker-ce docker-ce-cli containerd runc

# Remove Docker Compose if installed with apt
apt remove -y docker-compose

# Remove Docker Compose
rm /usr/local/bin/docker-compose
