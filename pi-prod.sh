#!/bin/bash

# Check if sudo
if [ "$SUDO_USER" == '' ]
then
    echo 'Please run as super user.'
    exit 1
fi

# Source config
. config.conf

# Install utilities
apt install -y make apt-transport-https ca-certificates curl software-properties-common gnupg-agent

# Docker:
# Use Docker convenience script on Raspbian
sudo -u $SUDO_USER curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Create docker group and add user
groupadd docker
usermod -aG docker $SUDO_USER

# Install Docker Compose
apt install -y docker-compose
