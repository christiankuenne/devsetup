#!/bin/bash

# Check if sudo
if [ "$SUDO_USER" == '' ]
then
    echo 'Please run as super user.'
    exit 1
fi

# Source config
. config.conf

# Get Distro (Debian/Ubuntu) and Release (buster/bionic)
DISTRO=$(echo "$(lsb_release -is)" | awk '{print tolower($0)}')
RELEASE=$(echo "$(lsb_release -cs)" | awk '{print tolower($0)}')

# Install utilities
apt install -y make apt-transport-https ca-certificates curl software-properties-common gnupg-agent

# Add Docker repo
sudo -u $SUDO_USER curl -fsSL http://download.docker.com/linux/$DISTRO/gpg | apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/$DISTRO $RELEASE stable" > /etc/apt/sources.list.d/docker.list

# Update and install
apt update
apt install -y docker-ce docker-ce-cli containerd.io

# Create docker group and add user
groupadd docker
usermod -aG docker $SUDO_USER

# Install Docker Compose
if [ ! -f /usr/local/bin/docker-compose ]
then
    curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
fi
